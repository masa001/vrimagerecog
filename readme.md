# はじめに

このアプリはUnity + Oculus riftで画像認識APIをたたいて結果を表示するものです。  
画像認識APIを動作させるには画像認識APIのAPIKEYをPlayerController.csに設定する必要があります。  
また、画像認識を行うためにはWebカメラが必要となります。  

# 動作確認環境

* Windows8.1
* Unity5.1.3f1
* Oculus Rift SDK 0.6.0.1(DK1)

# 操作方法

* R:視点をリセット
* E:テスト用（結果をそれっぽく表示、APIKEY不要）
* T:Webカメラ起動
* G:カメラ起動に撮影実行 + 画像認識開始

# 使用したライブラリとか

* MiniJSON
    * https://gist.github.com/darktable/1411710


