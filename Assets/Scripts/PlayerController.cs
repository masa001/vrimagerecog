﻿using UnityEngine;
using UnityEngine.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using MiniJSON;


public class PlayerController : MonoBehaviour {

	public GameObject mCenter;
	public GameObject mRight;
	public GameObject mLeft;

	public GameObject mCameraPanel;

	private WebCamTexture mWebCamTexture;

	private bool mCameraFlag;

	private byte[] mPicture;

	// Use this for initialization
	void Start () {
		mWebCamTexture =  new WebCamTexture (WebCamTexture.devices [0].name);
		mCameraPanel.GetComponent<Renderer> ().material.mainTexture = mWebCamTexture;
		mCameraFlag = false;
	}
		
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.R)) 
		{
			Debug.Log ("Press R Key");
			InputTracking.Recenter();
		}
		if(Input.GetKeyDown (KeyCode.E)) 
		{
			Debug.Log ("Press E Key");
			StartCoroutine("TestRecog");
		}

		// TODO ボタン連打対策する
		// カメラ起動
		if(Input.GetKeyDown (KeyCode.T)) 
		{
			Debug.Log ("Press T Key");
			if(!mCameraFlag) {
				mCameraFlag = true;
				StartCamera();
				resetPanels();
			}
		}
		// 撮影して画像認識を行う
		if(Input.GetKeyDown (KeyCode.G)) 
		{
			Debug.Log ("Press G Key");
			if(mCameraFlag) {
				mPicture = TakePngPicture();
				StopCamera();
				StartCoroutine ("Recog");
			}
		}
	}


	static readonly String URL = "https://api.apigw.smt.docomo.ne.jp/imageRecognition/v1/recognize";
	
	static readonly String APIKEY = "";
	static readonly String CATEGORY = "book";
	static readonly int NUM_OF_CANDIDATES = 3;
	
	IEnumerator TestRecog() {
		var www = new WWW ("http://ecx.images-amazon.com/images/I/51Q2GFgLFUL._BO2,204,203,200_PIsitb-sticker-v3-big,TopRight,0,-55_SX324_SY324_PIkin4,BottomRight,1,22_AA300_SH20_OU09_.jpg");
		yield return www;
		var tex = new Texture2D (480, 480);
		tex.LoadImage (www.bytes);
		setTexture (mCenter, tex);
		www.Dispose ();

		var www2 = new WWW ("http://img.7netshopping.jp/bks/images/i8/07215055.JPG");
		yield return www2;
		tex = new Texture2D (480, 480);
		tex.LoadImage (www2.bytes);
		setTexture (mRight, tex);
		www2.Dispose ();

		var www3 = new WWW ("http://ecx.images-amazon.com/images/I/51Q2GFgLFUL._BO2,204,203,200_PIsitb-sticker-v3-big,TopRight,0,-55_SX324_SY324_PIkin4,BottomRight,1,22_AA300_SH20_OU09_.jpg");
		yield return www3;
		tex = new Texture2D (480, 480);
		tex.LoadImage (www3.bytes);
		setTexture (mLeft, tex);
		www3.Dispose ();
	}

	// XXX 認識API叩く部分とUI書き換える部分を分離したいなぁ。
	IEnumerator Recog() {
		Debug.Log ("Recog");
		var tUrl = URL + "?APIKEY=" + APIKEY
					+ "&recog=" + CATEGORY
					+ "&numOfCandidates=" + NUM_OF_CANDIDATES;
		Dictionary<string,string> tHeaders = new Dictionary<string,string>();
		tHeaders.Add("Content-Type", "application/octet-stream");
		WWW www = new WWW(tUrl,mPicture,tHeaders);
		yield return www;
		if (www.error != null) {
			Debug.Log (www.error);
		}
		var result = www.text;
		Debug.Log(result);
		www.Dispose();
		mPicture = null;
		mCameraFlag = false;

		// 結果を表示する
		var json = Json.Deserialize (result) as Dictionary<String, object>;
		Debug.Log (json);
		foreach (var key in json.Keys) {
			Debug.Log (String.Format("Key : {0} / Value : {1}", key, json[key]));
			if(key == "candidates") {
				List<System.Object> candidates = (List<System.Object>)json[key];
				var count = 0;
				foreach (Dictionary<String, object> candidate in candidates) {
					Debug.Log (String.Format("Candidate : {0}", candidate));
					var imageUrl = (String) candidate["imageUrl"];
					Debug.Log(imageUrl);
					var imageWWW = new WWW( imageUrl );
					yield return imageWWW;
					var tex = new Texture2D( 480, 480 );
					tex.LoadImage( imageWWW.bytes );
					switch(count) {
					case 0:
						setTexture(mCenter, tex);
						break;
					case 1:
						setTexture(mRight, tex);
						break;
					case 2:
						setTexture(mLeft, tex);
						break;
					default:
						// とりあえず想定しない
						break;
					}
					imageWWW.Dispose();
					count++;
				}
			}
		}
	}

	void resetPanels() {
		var tex = new Texture2D( 480, 480 );
		setTexture (mCenter, tex);
		setTexture (mRight, tex);
		setTexture (mLeft, tex);
	}

	void setTexture(GameObject aPanel, Texture2D aTexture) {
		aPanel.GetComponent<Renderer> ().material.mainTexture = aTexture;
	}
	
	void StartCamera() {
		mWebCamTexture.Play();
		mCameraPanel.SetActive (true);
	}

	byte[] TakePngPicture() {
		var colors = mWebCamTexture.GetPixels32 ();
		var tTexture = new Texture2D (mWebCamTexture.width, mWebCamTexture.height);
		tTexture.SetPixels32 (colors);
		return tTexture.EncodeToPNG ();
	}
	
	void StopCamera() {
		mCameraPanel.SetActive (false);
		mWebCamTexture.Stop();
	}
}
